/*
 * Main window of the application including GUI-callbacks and point cloud visualizer.
 *
 * LUM02
 * @author Nils Dunkelberg
 * @author Ke Weiyao
 *
*/

#include <QTimer>
#include <QFileInfo>

#include <iostream>

#include "pclviewer.h"
#include "ui_pclviewer.h"

#include "Windows.h"

PCLViewer::PCLViewer (QWidget *parent) :
  QMainWindow (parent),
  ui (new Ui::PCLViewer),
  lumReg (new LumRegistration),
  active_cloud_set_(&input_clouds_col),
  ref_folder_("ref"),
  transl_unit_(" m"),
  rot_unit_(" °")

{
  ui->setupUi (this);
  this->setWindowTitle ("LUM Registration Testbed");

  // Set up the QVTK window
  viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));
  ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
  viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());
  ui->qvtkWidget->update ();

  // set labels of the input and output transformation display as greek symbols
  // \todo: set greek symbols directly via xml in pclviewer.ui
  ui->phi_input_label->setText(QString(QChar(0xc6, 0x03)) +  ":");
  ui->phi_output_label->setText(QString(QChar(0xc6, 0x03)) +  ":");
  ui->phi_ref_label->setText(QString(QChar(0xc6, 0x03)) +  ":");
  ui->theta_input_label->setText(QString(QChar(0xb8, 0x03)) +  ":");
  ui->theta_output_label->setText(QString(QChar(0xb8, 0x03)) +  ":");
  ui->theta_ref_label->setText(QString(QChar(0xb8, 0x03)) +  ":");
  ui->psi_input_label->setText(QString(QChar(0xc8, 0x03)) +  ":");
  ui->psi_output_label->setText(QString(QChar(0xc8, 0x03)) +  ":");
  ui->psi_ref_label->setText(QString(QChar(0xc8, 0x03)) +  ":");

  // connect load button
  connect (ui->loadPCButton, SIGNAL (clicked()), this, SLOT (loadPointClouds ()));

  // connect visualization toggle buttons (each for input clouds resp. transformed clouds)
  connect (ui->showInputBtn, SIGNAL (clicked()), this, SLOT (showInputClouds ()));
  connect (ui->showOutputBtn, SIGNAL (clicked()), this, SLOT (showTransformedClouds ()));
  connect (ui->showCoordBtn, SIGNAL (clicked()), this, SLOT (showCoordSystems ()));
  connect (ui->showLinkBtn, SIGNAL (clicked()), this, SLOT (showNetworkEdges ()));
  connect (ui->showScaleGridBtn, SIGNAL(clicked()), this, SLOT(showScaleGrid()));
  connect (ui->resetCameraBtn, SIGNAL(clicked()), this, SLOT(resetCamera()));

  // connect button with launch callback for lum registration
  connect (ui->startLumBtn, SIGNAL (clicked()), this, SLOT (performLumRegistration ()));

  // connect lists of input and output point clouds with a rowChanged callback
  // the tranformation of each cloud will be displayed individually, thus.
  connect(ui->inputPointCloudList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( inputListItemClicked(QListWidgetItem*)));
  connect(ui->outputPointCloudList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( outputListItemClicked(QListWidgetItem*)));

  // connect progress update
  connect (lumReg, &LumRegistration::progressUpdate, this, &PCLViewer::updateProgress);

  //make directories for input and output pointclouds
  ted_path_ = QDir::currentPath() + QDir::separator() + ".." + QDir::separator() + "ted";
  tad_path_ = QDir::currentPath() + QDir::separator() + ".." + QDir::separator() + "tad";
  checkDirectory(ted_path_);
  checkDirectory(tad_path_);

  // show scale grid (needs to be called after constructor finished, since qt gui can't accept key events prior to initialization)
  QTimer::singleShot(200, this, SLOT(showScaleGrid()));
}

void PCLViewer::loadPointClouds() {
    // open file dialog in the relative ted directory
    QStringList filenames = QFileDialog::getOpenFileNames(this,tr("PCD files"), ted_path_,tr("PCD files (*.pcd)") );
    if( !filenames.isEmpty() )
    {
        //flush previous input
        input_clouds.clear();
        input_clouds_col.clear();
        ref_trafos_.clear();

        //flush previous output
        transformed_clouds_.clear();

        // count points
        int point_counter = 0;

        for (int i =0;i<filenames.count();i++) {
            std::string fname_str = filenames.at(i).toLocal8Bit().constData();
            //store plain point clouds for lum algorithm
            CloudPtr pc (new Cloud);
            pcl::io::loadPCDFile (fname_str, *pc);
            input_clouds.push_back (CloudPair (fname_str, pc));

            //store colored point clouds for visualization
            CloudColPtr pc_col(new CloudCol);
            copyPointCloud(*pc, *pc_col);
            input_clouds_col.push_back(CloudColPair (fname_str, pc_col));

            // add point amount
            point_counter += pc->size();
        }
        printf("%i point clouds added \n", filenames.count());

        // disable lum calculation, if less then two point clouds loaded
        if (input_clouds.size() < 2) {
            ui->startLumBtn->setEnabled(false);
        }
        else {
            ui->startLumBtn->setEnabled(true);
        }

        // attempt to load reference transformations
        loadReferenceTransformations();

        // visualize loaded point clouds
        showInputClouds();

        // add point clouds to lum instance
        addCloudsToList();

        //display total amount of points
        ui->total_point_disp->setText(QString::number(point_counter));

        // reset coord system and network link visualization
        ui->showCoordBtn->setChecked(false);
        ui->showLinkBtn->setChecked(false);
        showCoordSystems();
        showNetworkEdges();

        // reset camera
        resetCamera();
    }
}

void PCLViewer::loadReferenceTransformations() {
    if (input_clouds.empty()) {
        return;
    }

    // get input clouds directory
    std::string filepath_tmpl = input_clouds[0].first;

    // cut filename and add reference-folder to the path
    QString ref_dir = QString::fromStdString(filepath_tmpl.substr (0, filepath_tmpl.rfind ("/") + 1)) + ref_folder_;

    // check if path available
    QDir dir(ref_dir);
    if (!dir.exists()) {
        return;
    }

    for (size_t i = 0; i < input_clouds.size(); i++) {
        std::string filepath = input_clouds[i].first;
        filepath.insert(filepath.rfind ("/") + 1, (ref_folder_ + "/").toLocal8Bit().constData());

        // create empty transformation
        Eigen::Vector6f ref_tf;
        for (int j = 0; j < 6; j++ ) ref_tf(j) = 0;

        // if reference file exists, load reference transformation, otherwise add empty transformation
        QFileInfo file(QString::fromStdString(filepath));
        if (file.exists()) {
            // load reference point cloud
            CloudPtr pc (new Cloud);
            pcl::io::loadPCDFile (filepath, *pc);

            // retrieve transformation
            Eigen::Vector4f origin = pc->sensor_origin_;
            Eigen::Quaternionf orientation = pc->sensor_orientation_;

            // retrieve euler angles from quaternion (xyz -convention) in degree
            auto euler_angles = orientation.toRotationMatrix().eulerAngles(0, 1, 2);
            euler_angles *= 180.0f/M_PI;

            ref_tf.head(3) = origin.head(3);
            ref_tf.tail(3) = euler_angles;
        }

        // add reference transformation
        ref_trafos_.push_back(ref_tf);
    }
}

void PCLViewer::visualizePointClouds() {
    //flush visualization
    viewer->removeAllPointClouds();
    for (size_t i = 0; i < active_cloud_set_->size (); i++) {
        //apply random color
        uint r = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
        uint g = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
        uint b = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
        CloudColPtr pc = (*active_cloud_set_)[i].second;
        for (size_t i = 0; i < pc->size (); i++)
          {
            pc->points[i].r = r;
            pc->points[i].g = g;
            pc->points[i].b = b;
          }
        //update if viewer already holds pointcloud otherwise add to it
        if (!viewer->updatePointCloud (pc, (*active_cloud_set_)[i].first)) {
            viewer->addPointCloud(pc, (*active_cloud_set_)[i].first);
        }
    }
    //update visualization
    ui->qvtkWidget->update ();
}

void PCLViewer::addCloudsToList() {
    // clear list-widgets of input and output clouds
    clearListWidgets();

    // add newly loaded point clouds to the input list of the gui
    for (size_t i = 0; i < input_clouds.size (); i++) {
        auto *item = new QListWidgetItem();
        // get filepath from stored cloud pair to name list entry
        std::string filepath = input_clouds[i].first;
        // check if cloud contained in ted-directory, if so apply subsequent filepath, otherwise apply full path
        size_t checkTed = filepath.find("ted");
        std::string listEntry = checkTed == std::string::npos ? filepath : filepath.substr(filepath.find("ted"));
        // add resulting point cloud path to list of input clouds
        item->setText(QString::number(i).rightJustified(2, '0') + ":  " + QString::fromStdString(listEntry));
        ui->inputPointCloudList->addItem(item);
    }

}

void PCLViewer::showInputClouds() {
    ui->showOutputBtn->setChecked(false);
    ui->showInputBtn->setChecked(true);
    // set cloud set to be visualized
    active_cloud_set_ = &input_clouds_col;
    visualizePointClouds();
}

void PCLViewer::showTransformedClouds() {
    ui->showInputBtn->setChecked(false);
    ui->showOutputBtn->setChecked(true);

    // set cloud set to be visualized
    active_cloud_set_ = &transformed_clouds_;
    visualizePointClouds();
}

void PCLViewer::showScaleGrid() {
    // work around: apply "g"-key-press on qvtk-widget (since pcl viewer doesn't provide dedicated public function to show grid)
    ui->qvtkWidget->setFocus();

    // Set up a keyboard event (press and release of "g" key).
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wVk = 0x47; // virtual-key code for the "g" key
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));
}

void PCLViewer::resetCamera() {
    viewer->resetCamera ();
    ui->qvtkWidget->update();
}

void PCLViewer::performLumRegistration() {
    // return if no multiple input clouds loaded
    if (input_clouds.size() < 2) return;

    //add to lum registration backend
    lumReg->setInputClouds(input_clouds);

    // clear list of previously transformed clouds (gui)
    ui->outputPointCloudList->clear();

    printf("performing lum registration algorithm \n");

    //reset progress bar
    ui->lumProgress->setValue(ui->lumProgress->minimum());

    // retrieving registration parameters from gui
    uint iter = ui->iterationSpinBox->value();
    double conv_th = ui->convThSpinBox->value();
    double corr_dist = ui->corrDistSpinBox->value();
    double link_th = ui->linkDistSpinBox->value();
    // launch computation
    lumReg->compute(iter, conv_th, corr_dist, link_th);

    //retrieve transformed point clouds
    CloudVector result;
    lumReg->getTransformedClouds(result);
    // store result as colored point clouds for visualization
    for (size_t i = 0; i < result.size (); i++) {
        CloudColPtr pc_col(new CloudCol);
        copyPointCloud(*result[i].second, *pc_col);

        // now create a new filename
        // if input clouds are in  the "ted-directory", store transformed clouds in the "tad-directory" with equal subsequent directory structure
        // otherwise, store transformed clouds in same directory with "transformed_" prefix within each filename
        // get filepath from stored cloud pair
        std::string filepath = result[i].first;
        // check if cloud contained in ted-directory
        size_t checkTed = filepath.find("ted");
        // if so, replace ted with tad to retrieve storage path
        if (checkTed != std::string::npos) {
            filepath.replace(checkTed, 3, "tad");
            std::string path_without_filename = filepath.substr (0, filepath.rfind ("/") + 1);
            checkDirectory(QString::fromStdString(path_without_filename));
        }
        else {
            filepath.insert(filepath.rfind ("/") + 1, "transformed_");
        }

        // push cloud to list of transformed clouds
        transformed_clouds_.push_back(CloudColPair (filepath, pc_col));
        pcl::io::savePCDFileBinary (filepath.c_str (), *pc_col);

        // add new entry in the gui -list of transformed point clouds
        auto *item = new QListWidgetItem();
        std::string listEntry = checkTed == std::string::npos ? filepath : filepath.substr(filepath.find("tad"));
        // add resulting point cloud path to list of input clouds
        item->setText(QString::number(i).rightJustified(2, '0') + ":  " + QString::fromStdString(listEntry));
        ui->outputPointCloudList->addItem(item);
    }

    // retrieve network edges
    lumReg->getNetworkEdges(network_edges_);

    //visualize
    showTransformedClouds();
    showNetworkEdges();
    showCoordSystems();
    showComputationTime();

}

void PCLViewer::updateProgress(int progress) {
    ui->lumProgress->setValue(progress);
}

void PCLViewer::showCoordSystems() {
    viewer->removeAllCoordinateSystems();
    if (!ui->showCoordBtn->isChecked()) {
        //update visualization
        ui->qvtkWidget->update ();
        return;
    }

    viewer->addCoordinateSystem(1, "global_frame");

    if (active_cloud_set_ == &transformed_clouds_) {
        for (size_t i = 1; i < transformed_clouds_.size(); i++) {
            Eigen::Affine3f transformation;
            lumReg->getTransformation(transformation, i);
            QString id = "frame_" + QString::number(i);
            std::string id_str = id.toUtf8().constData();
            if (!viewer->updateCoordinateSystemPose(id_str, transformation))
                    viewer->addCoordinateSystem(0.5, transformation, id_str);
        }
    }

    //update visualization
    ui->qvtkWidget->update ();

}

void PCLViewer::showNetworkEdges() {

    viewer->removeAllShapes();
    if (!ui->showLinkBtn->isChecked()) {
        //update visualization
        ui->qvtkWidget->update ();
        return;
    }

    if (active_cloud_set_ != &transformed_clouds_) return;

    for (size_t i = 0; i < network_edges_.size (); i++) {
        uint cloud_index_1 = network_edges_[i].first;
        uint cloud_index_2 = network_edges_[i].second;
        CloudColPtr c1 = (*active_cloud_set_)[cloud_index_1].second;
        CloudColPtr c2 = (*active_cloud_set_)[cloud_index_2].second;

        Eigen::Affine3f transformation_1;
        Eigen::Affine3f transformation_2;
        lumReg->getTransformation(transformation_1, cloud_index_1);
        lumReg->getTransformation(transformation_2, cloud_index_2);

        Eigen::Vector3f origin_1 = transformation_1.translation();
        Eigen::Vector3f origin_2 = transformation_2.translation();

        PointType p1;
        p1.getVector3fMap() = origin_1;

        PointType p2;
        p2.getVector3fMap() = origin_2;

        QString id = "link_" + QString::number(i);
        viewer->addLine(p1, p2, 1.0, 0.5, 0.5, id.toUtf8().constData());
    }

    //update visualization
    ui->qvtkWidget->update ();
}

void PCLViewer::inputListItemClicked(QListWidgetItem *item) {
    // load transformation
    int index = ui->inputPointCloudList->currentRow();
    CloudPtr pc = input_clouds[index].second;

    // retrieve translation and orientation of the point cloud origin
    Eigen::Vector4f origin = pc->sensor_origin_;
    Eigen::Quaternionf orientation = pc->sensor_orientation_;

    // retrieve euler angles from quaternion (xyz -convention) in degree
    auto euler_angles = orientation.toRotationMatrix().eulerAngles(0, 1, 2);
    euler_angles *= 180.0f/M_PI;

    // update transformation display panel
    ui->x_input_disp->setText(QString::number(origin.x(), 'f', 4) + transl_unit_);
    ui->y_input_disp->setText(QString::number(origin.y(), 'f', 4) + transl_unit_);
    ui->z_input_disp->setText(QString::number(origin.z(), 'f', 4) + transl_unit_);
    ui->phi_input_disp->setText(QString::number(euler_angles[0], 'f', 4) + rot_unit_);
    ui->theta_input_disp->setText(QString::number(euler_angles[1], 'f', 4) + rot_unit_);
    ui->psi_input_disp->setText(QString::number(euler_angles[2], 'f', 4) + rot_unit_);

    // show reference transformation if available
    if (!ref_trafos_.empty()) {
        ui->x_ref_disp->setText(QString::number(ref_trafos_[index][0], 'f', 4) + transl_unit_);
        ui->y_ref_disp->setText(QString::number(ref_trafos_[index][1], 'f', 4) + transl_unit_);
        ui->z_ref_disp->setText(QString::number(ref_trafos_[index][2], 'f', 4) + transl_unit_);
        ui->phi_ref_disp->setText(QString::number(ref_trafos_[index][3], 'f', 4) + rot_unit_);
        ui->theta_ref_disp->setText(QString::number(ref_trafos_[index][4], 'f', 4) + rot_unit_);
        ui->psi_ref_disp->setText(QString::number(ref_trafos_[index][5], 'f', 4) + rot_unit_);
    }

    // display amount of points of this cloud
    ui->point_disp->setText(QString::number(pc->size()));

}

void PCLViewer::outputListItemClicked(QListWidgetItem *item) {
    int index = ui->outputPointCloudList->currentRow();
    Eigen::Affine3f lum_transformation;
    lumReg->getTransformation(lum_transformation, index);

    //retrieve trsnlation and orientation of the point cloud origin
    Eigen::Matrix3f lum_rot_mat = lum_transformation.rotation();
    Eigen::Vector3f lum_origin = lum_transformation.translation();

    // retrieve euler angles from quaternion (xyz -convention) in degree
    auto euler_angles = lum_rot_mat.eulerAngles(0, 1, 2);
    euler_angles *= 180.0f/M_PI;

    // update transformation display panel
    ui->x_output_disp->setText(QString::number(lum_origin.x(), 'f', 4) + transl_unit_);
    ui->y_output_disp->setText(QString::number(lum_origin.y(), 'f', 4) + transl_unit_);
    ui->z_output_disp->setText(QString::number(lum_origin.z(), 'f', 4) + transl_unit_);
    ui->phi_output_disp->setText(QString::number(euler_angles[0], 'f', 4) + rot_unit_);
    ui->theta_output_disp->setText(QString::number(euler_angles[1], 'f', 4) + rot_unit_);
    ui->psi_output_disp->setText(QString::number(euler_angles[2], 'f', 4) + rot_unit_);

    // calculate errors
    if(ref_trafos_.empty()) {
        return;
    }

    //translation error
    Eigen::Vector3f ref_origin = ref_trafos_.at(index).head(3);
    Eigen::Vector3f distance = ref_origin - lum_origin;
    float transl_error = distance.norm();

    ui->error_trans_disp->setText(QString::number(transl_error, 'f', 4));

    // rotation error
    // convert reference euler angles to translation matrix (xyz-convention) with positional part to zero
    Eigen::Vector3f ref_euler_angles = M_PI/180.0f * ref_trafos_[index].tail(3);
    Eigen::Affine3f ref_trans_mat =  pcl::getTransformation(0, 0, 0, ref_euler_angles[0], ref_euler_angles[1], ref_euler_angles[2]);

    Eigen::Matrix3f ref_rot_mat = ref_trans_mat.rotation();

    EulerParams ref_ep = calculateEulerParams(ref_rot_mat);
    EulerParams lum_ep = calculateEulerParams(lum_rot_mat);

    // the difference between the euler (param) angles from reference and estimated transformation
    float euler_diff = ref_ep.eulerAngle - lum_ep.eulerAngle;

    // the angle between the euler axes of the reference and the estimated rotation
    float euler_axis_angle ;
    if(ref_ep.eulerAxis.norm() == 0 || lum_ep.eulerAxis.norm() == 0) {
        // if the euler axis norm is 0 (set by calculateEulerParams) then the difference angle between the axes can't be measured
        euler_axis_angle = 0;
    }
    else {
        euler_axis_angle = acos(ref_ep.eulerAxis.dot(lum_ep.eulerAxis) / (ref_ep.eulerAxis.norm() * lum_ep.eulerAxis.norm()));
    }

    // convert to degree
    float rot_angle_error = euler_diff  * 180/M_PI;
    float rot_axis_error = euler_axis_angle * 180 / M_PI;
    float rot_error = sqrt(pow(rot_angle_error,2) + pow(rot_axis_error,2));

    //display the rotation error
    ui->error_rot_angle_disp->setText(QString::number(rot_error, 'f', 4));

}

EulerParams PCLViewer::calculateEulerParams(Eigen::Matrix3f &rot_mat) {
    EulerParams ep;

    // get euler parameters from reference rotation matrix
    ep.eulerAngle = acos(0.5 * (rot_mat(0,0) + rot_mat(1,1) + rot_mat(2,2) -1));

    if (ep.eulerAngle == 0) {
        // set to zero -> no euler axis error there
        ep.eulerAxis << 0, 0, 0;
        return ep;
    }
    else if (ep.eulerAngle == M_PI) {
        if (rot_mat(0,0) == 1) ep.eulerAxis(0) = 1;
        else if (rot_mat(1,1) == 1) ep.eulerAxis(1) = 1;
        else if (rot_mat(2,2) == 1) ep.eulerAxis(2) = 1;
        return ep;
    }

    Eigen::Vector3f v;
    v(0) = rot_mat(2,1) - rot_mat(1,2);
    v(1) = rot_mat(0,2) - rot_mat(2,0);
    v(2) = rot_mat(1,0) - rot_mat(0,1);

    ep.eulerAxis = 0.5/sin(ep.eulerAngle) * v;

    return ep;
}

void PCLViewer::checkDirectory(QString path) {
    QDir dir(path);
      if (!dir.exists() && !dir.mkpath(".")) {
        throw std::runtime_error("Failed to create cache folder: " +
                                 path.toStdString());
    }
}

void PCLViewer::showComputationTime() {
    int comp_time = lumReg->getTotalComputationTime();
    int hours = std::floor(comp_time / 3600000);
    int minutes = std::floor((comp_time - hours * 3600000) / 60000);
    int seconds = std::floor((comp_time - hours * 3600000 - minutes * 60000) / 1000);
    int milliseconds = std::floor(comp_time - hours * 3600000 - minutes * 60000 - seconds * 1000);

    QString hours_str = QString::number(hours).rightJustified(2, '0');
    QString minutes_str = QString::number(minutes).rightJustified(2, '0');
    QString seconds_str = QString::number(seconds).rightJustified(2, '0');
    QString milliseconds_str = QString::number(milliseconds).rightJustified(3, '0');

    ui->comp_time_disp->setText(hours_str + ":" + minutes_str + ":" + seconds_str + ":" + milliseconds_str);



}

void PCLViewer::clearListWidgets() {
    ui->inputPointCloudList->clear();
    ui->outputPointCloudList->clear();

    ui->x_input_disp->setText("");
    ui->y_input_disp->setText("");
    ui->z_input_disp->setText("");
    ui->phi_input_disp->setText("");
    ui->theta_input_disp->setText("");
    ui->psi_input_disp->setText("");

    ui->x_ref_disp->setText("");
    ui->y_ref_disp->setText("");
    ui->z_ref_disp->setText("");
    ui->phi_ref_disp->setText("");
    ui->theta_ref_disp->setText("");
    ui->psi_ref_disp->setText("");

    ui->x_output_disp->setText("");
    ui->y_output_disp->setText("");
    ui->z_output_disp->setText("");
    ui->phi_output_disp->setText("");
    ui->theta_output_disp->setText("");
    ui->psi_output_disp->setText("");

    ui->comp_time_disp->setText("");
    ui->error_rot_angle_disp->setText("");
    ui->error_trans_disp->setText("");

    ui->total_point_disp->setText("");
    ui->point_disp->setText("");

}

PCLViewer::~PCLViewer ()
{
  delete ui;
}
