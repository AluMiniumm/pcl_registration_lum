/*
 * Contains the main header files and some type definations to avoid nested namespace expressions
 *
 * LUM02
 * @author Nils Dunkelberg
 * @author Ke Weiyao
 *
*/

#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// simplify standard pcl classes
typedef pcl::PointXYZ PointType;
typedef pcl::PointCloud<PointType> Cloud;
typedef Cloud::ConstPtr CloudConstPtr;
typedef Cloud::Ptr CloudPtr;
typedef std::pair<std::string, CloudPtr> CloudPair;
typedef std::vector<CloudPair> CloudVector;

// same for colored clouds
typedef pcl::PointXYZRGB PointTypeCol;
typedef pcl::PointCloud<PointTypeCol> CloudCol;
typedef CloudCol::Ptr CloudColPtr;
typedef std::pair<std::string, CloudColPtr> CloudColPair;
typedef std::vector<CloudColPair> CloudColVector;

typedef unsigned int uint;

typedef std::pair<uint, uint> NetworkEdge;
typedef std::vector<NetworkEdge> NetworkEdges;

typedef struct EulerParams {
    Eigen::Vector3f eulerAxis;
    float eulerAngle;
};


#endif // COMMON_TYPES_H
