/*
 * Contains the calculation back end of the lum registration algorithm.
 *
 * LUM02
 * @author Nils Dunkelberg
 * @author Ke Weiyao
 *
*/

#ifndef LUMREGISTRATION_H
#define LUMREGISTRATION_H

#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/lum.h>
#include <pcl/registration/correspondence_estimation.h>

#include <QObject>
#include <QTime>

#include "common_types.h"

class LumRegistration : public QObject
{
    Q_OBJECT
public:
    LumRegistration();

    /*!
     * Starts the main loop of the point cloud registration.
     * \param iteration Number of overall iterations.
     * \param conv_th Convergence threshold beneath which lum registration gets cancelled.
     * \param corr_dist Maximum distance, which corresponding points are allowed to obtain.
     * \param link_th Maximum distance, which to linked object frames are allowed to obtain.
     */
    void
    compute(uint iteration, double conv_th, double corr_dist, double link_th);

    /*!
     * Sets the dataset of input point clouds, which the lum registration algorithm gets applied on
     * \param input_clouds Vector of CloudPairs containing each point cloud and its name
     */
    void
    setInputClouds(CloudVector input_clouds);

    /*!
     * Sets a reference to the transformed point clouds
     * \param transformed_clouds_ reference to the transformed CloudVector
     */
    void
    getTransformedClouds(CloudVector &transformed_clouds);

    /*!
     * Sets a reference to list of applied network edges
     * \param network_edges reference to the list of applied network edges
     */
    void
    getNetworkEdges(NetworkEdges &network_edges);

    /*!
     * \brief getTransformation Retrieves the applied transformation of the point cloud with given index after registration
     * \param transformation Reference to the resulting transformation of the point cloud
     * \param index Index of the requested point cloud in the SLAM Graph
     */
    void
    getTransformation(Eigen::Affine3f &transformation, int index);

    /*!
     * \brief getTotalComputationTime Returns the total duration of the lum computation in milliseconds
     * \return Computing time in milliseconds
     */
    int
    getTotalComputationTime();

public:
Q_SIGNALS:
    /*!
     * Slot to update the progress bar during computation
     * \param progress Progress of the computation percent
     */
    void progressUpdate(int progress);


protected:

private:
    boost::shared_ptr<pcl::registration::LUM<PointType>> lum; ///< registration backend containing the implementation of lum registration algorithm
    CloudVector proc_clouds_; ///< contains the processing point clouds which get iteratively optimized
    NetworkEdges edges_; ///< contains indizes of each pair of linked point clouds in a vector.

    int total_computation_time_; ///< stores the total time of computation.
};

#endif // LUMREGISTRATION_H
