/*
 * Contains the calculation back end of the lum registration algorithm.
 *
 * LUM02
 * @author Nils Dunkelberg
 * @author Ke Weiyao
 *
*/

#include "lumregistration.h"

// empty constructor
LumRegistration::LumRegistration()
{
}

void
LumRegistration::compute(uint iteration, double conv_th, double corr_dist, double link_th) {
    lum->setConvergenceThreshold(conv_th);

    // main loop
    // store starting time stamp
    QTime computation_timer;
    computation_timer.start();
    for (int i = 0; i < iteration; i++)
     {
       // clear list of edges
       edges_.clear();
       // find correspondences
       for (size_t i = 1; i < proc_clouds_.size (); i++)
         for (size_t j = 0; j < i; j++)
         {
           Eigen::Vector4f ci, cj;
           pcl::compute3DCentroid (*(proc_clouds_[i].second), ci);
           pcl::compute3DCentroid (*(proc_clouds_[j].second), cj);
           Eigen::Vector4f diff = ci - cj;

           if(diff.norm () < link_th || i - j == 1)
           {
             pcl::registration::CorrespondenceEstimation<PointType, PointType> ce;
             ce.setInputTarget (proc_clouds_[i].second);
             ce.setInputSource (proc_clouds_[j].second);
             pcl::CorrespondencesPtr corr (new pcl::Correspondences);
             ce.determineCorrespondences (*corr, corr_dist);
             if (corr->size () > 2)
               lum->setCorrespondences (j, i, corr);
               // add edge to list of edges
               edges_.push_back(NetworkEdge (i, j));
           }
         }

       lum->compute ();

       for(size_t i = 0; i < lum->getNumVertices (); i++)
       {
         // retrieve transformed cloud
         proc_clouds_[i].second = lum->getTransformedCloud (i);
       }

       // emit progress to update progress bar
       int progress = std::floor(100*(i+1)/(float)iteration);
       Q_EMIT progressUpdate(progress);
   }

   total_computation_time_ = computation_timer.elapsed();

}

void
LumRegistration::setInputClouds(CloudVector input_clouds) {
    proc_clouds_ = input_clouds;

    // reset lum instance since new dataset is used (registration::lum does not offer possibility to flush slam graph atm)
    lum.reset (new pcl::registration::LUM<PointType>());

    // add each point cloud to lum registration backend
    for (int i = 0; i < proc_clouds_.size(); i++) {
        lum->addPointCloud(proc_clouds_[i].second);
    }
}

void
LumRegistration::getTransformedClouds(CloudVector &transformed_clouds) {
    transformed_clouds = proc_clouds_;
}

void
LumRegistration::getTransformation(Eigen::Affine3f &transformation, int index) {
    transformation = lum->getTransformation(index);
}

void
LumRegistration::getNetworkEdges(NetworkEdges &network_edges) {
    network_edges = edges_;
}

int
LumRegistration::getTotalComputationTime() {
    return total_computation_time_;
}
