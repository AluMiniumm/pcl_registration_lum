#-------------------------------------------------
#
# Project created by QtCreator 2014-05-01T14:24:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lum_testbed
TEMPLATE = app


SOURCES += main.cpp\
        pclviewer.cpp \
    lumregistration.cpp

HEADERS  += pclviewer.h \
    lumregistration.h \
    common_types.h

FORMS    += pclviewer.ui
