/*
 * Main window of the application including GUI-callbacks and point cloud visualizer.
 *
 * LUM02
 * @author Nils Dunkelberg
 * @author Ke Weiyao
 *
*/

#ifndef PCLVIEWER_H
#define PCLVIEWER_H

#include <iostream>

// Qt
#include <QMainWindow>
#include <QFileDialog>
#include <QListWidgetItem>

// Point Cloud Library
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

// std
#include <iostream>
#include <string>

#include <vector>

#include "common_types.h"
#include "lumregistration.h"

namespace Ui
{
  class PCLViewer;
}

class PCLViewer : public QMainWindow
{
  Q_OBJECT

public:
  explicit PCLViewer (QWidget *parent = 0);
  ~PCLViewer ();

public Q_SLOTS:

private Q_SLOTS:
    /*!
     * Opens file dialog to select input point clouds as .pcd format
     */
    void
    loadPointClouds();

    /*!
     * \brief Looks after "reference" directory within the directory of input clouds and loads reference transformations
     */
    void
    loadReferenceTransformations();

    /*!
     * Add loaded point clouds to the list widget of the graphical user interface
     */
    void
    addCloudsToList();

    /*!
     * Toggle visualization to show the raw input point clouds
     */
    void
    showInputClouds();

    /*!
     * Toggle visualization to show the transformed output point clouds
     */
    void
    showTransformedClouds();

    /*!
     * Toggle visibility of the edges of each pair of linked point clouds.
     */
    void
    showNetworkEdges();

    /*!
     * Toggle visibility of the object frames (frame origin and orientation) of each cloud.
     */
    void
    showCoordSystems();

    /*!
     * \brief Resets the camera of the 3D-view such that whole point cloud set is visible.
     */
    void
    resetCamera();

    /*!
     * \brief Displays a scale grid on the pcl-viewer display.
     */
    void
    showScaleGrid();

    /*!
     * Applies the lum registration algorithm to the loaded point clouds
     */
    void
    performLumRegistration();

    /*!
     * Slot to update the progress bar during computation
     * \param progress Progress of the computation percent
     */
    void
    updateProgress(int progress);

    /*!
     * \brief inputListRowChanged Will be called if the selected point cloud in the input list is changed
     * \param item Pointer to the selected item
     */
    void
    inputListItemClicked(QListWidgetItem *item);

    /*!
     * \brief outputListRowChanged Will be called if the selected point cloud in the output list is changed
     * \param index Pointer to the selected item
     */
    void
    outputListItemClicked(QListWidgetItem *item);

    /*!
     * \brief calculateEulerParams Will be called when a specific transformed output cloud is clicked.
     * \param rot_mat Rotation matrix which euler parameters are to be retrieved from.
     * \return euler parameters consisting of an euler axis and an euler angle
     */
    EulerParams
    calculateEulerParams(Eigen::Matrix3f &rot_mat);


protected:
    /*!
     * Colors and visualizes given pointclouds.
     */
    void
    visualizePointClouds();

    /*!
     * \brief checkDirectory Checks the specified directory for existance and throws error otherwise.
     * \param path Path to the directory
     */
    void
    checkDirectory(QString path);

    /*!
     * \brief clearListWidgets Clears the (gui) lists of input and output clouds and resets the displays for selected transformations
     */
    void
    clearListWidgets();

    /**
     * @brief showComputationTime Retrieves the computation time of the last registration and displays it.
     */
    void
    showComputationTime();

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer; ///< instance of 3d visualizer
  CloudVector input_clouds; ///< contains the initially loaded raw point clouds
  CloudColVector input_clouds_col; ///< contains colored input point clouds for visualization

  CloudColVector transformed_clouds_; ///< contains the already processed and colored clouds

  CloudColVector *active_cloud_set_; ///< holds a reference to either input or transformed clouds depending on which set is currently visualized

  std::vector<Eigen::Vector6f> ref_trafos_; ///< contains the reference transformations (x y z, r, p, y) [xyz-euler convention]

  NetworkEdges network_edges_; ///< contains indizes of each pair of linked point clouds in a vector.

private:
  Ui::PCLViewer *ui;
  LumRegistration *lumReg; ///< contains the computation backend

  QString transl_unit_; ///< Unit of translations (only for gui)
  QString rot_unit_; ///< Unit of rotations (only for gui)

  QString ted_path_; ///< Default path for input clouds
  QString tad_path_; ///< Default path for output clouds
  QString ref_folder_; ///< contains the name which the folder with reference transformations needs to have


};

#endif // PCLVIEWER_H
