### Installation instructions for linux

1. Install QT
    + sudo apt-get install qt5-default 
2. Install QT-Creator
    + sudo apt-get install qtcreator 
3. Go to arbitrary directory where pcl shall be installed
4. Clone PCL Repository
    + sudo git clone https://github.com/PointCloudLibrary/pcl.git
5. Go to repository
6. Create build folder
    + mkdir build
7. Go to build folder
    + cd build
8. Compile PCL (release mode)
    + sudo cmake -DCMAKE_BUILD_TYPE=Release ..
9. Install
    + sudo make -j2
    + sudo make -j2 install
10. Clone this repository to arbitrary directory
    + git clone https://gitlab.com/AluMiniumm/pcl_registration_lum.git
11. Launch QT Creator
12. Open src/lum_testbed.pro file from this repository
13. Go to "projects" tab on left
14. Configure as depicted here
    + __http://pointclouds.org/documentation/tutorials/qt_visualizer.php__
15. Build (Hammer Symbol to the bottom left)
16. Run (Green Arrow)
17. =)
